var website = angular.module('website', ['ui.router']);


website.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('form', {
        url: "/form",
        templateUrl: "/resources/form.html",
        controller: "appController"
    })
});

website.controller('appController', function ($scope) {
    $scope.addressToAdd = "";

    $scope.addAddress = function (addressToAdd) {
        console.log(addressToAdd);
    };
});



