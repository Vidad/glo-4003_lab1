<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html ng-app="website">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/resources/css/main.css" media="all"/>
    <title>Titre de la page</title>

    <!--Scripts-->
    <!--Vendors-->
    <script src="resources/scripts/vendors/angular.js"></script>
    <script src="resources/scripts/vendors/angular-ui-router.js"></script>
    <!--Custom-->
    <script src="resources/scripts/app.js"></script>

</head>
<body>

<button ui-sref="form">Ajouter une entrée</button>

<div ui-view></div>

</body>
</html>