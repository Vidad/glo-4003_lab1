package ca.ulaval.glo4003.web.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AddressController {

    @RequestMapping(value = "/addresses", method = RequestMethod.GET )
    public List<String> getAddresses(Model model){
        ArrayList<String> listOfAddresses = new ArrayList<String>();
        listOfAddresses.add("Loin");
        listOfAddresses.add("Ici");
        listOfAddresses.add("L�-bas");

        return listOfAddresses;
    }
}
